/**
 *
 * @author Froy
 * @since 1.0
 * @version 1.0
 */
public class HelloWorld {
    
    /**
     * main() es un método de entrada utilizado por el compilador de Java para ejecutar una aplicación Java.
     * public: Es un modificador de acceso que permite acceder a los recursos sin importar el paquete.
     * static: Es un modificador de clase que permite acceder al recurso sin necesidad de crear una instancia.
     * void: Es una palabra reservada que indica que no devuelve ningún tipo de dato. 
     * String[] args: Los argumentos que se reciben 
     * @param args 
     */
    public static void main(String[] args) {//Es un método de entrada para ejecutar uns aplicación Java.
        System.out.println("Hello world");
    }
    
}
